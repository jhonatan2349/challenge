package com.developer.challenge.exception;

import com.developer.challenge.domain.response.ErrorMessage;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.sql.SQLException;
import java.util.Date;

@ControllerAdvice
public class HandleException {
    @ExceptionHandler(value = {SQLException.class})
    public ResponseEntity<?> handler(SQLException ex, HttpServletRequest request) {
        return ResponseEntity.badRequest().body(ErrorMessage.builder()
                .timestamp(new Date())
                .code(HttpStatus.BAD_REQUEST)
                .status(HttpServletResponse.SC_BAD_REQUEST)
                .url(request.getRequestURL().toString())
                .errors(ex.getMessage()).build());
    }

    @ExceptionHandler(value = {CustomException.class})
    public ResponseEntity<?> handler(CustomException ex, HttpServletRequest request){
        return ResponseEntity.badRequest().body(ErrorMessage.builder()
                .timestamp(new Date())
                .code(HttpStatus.NOT_FOUND)
                .status(HttpServletResponse.SC_BAD_REQUEST)
                .url(request.getRequestURL().toString())
                .errors(ex.getMessage()).build());
    }

    @ExceptionHandler(value = {NoResourceFoundException.class})
    public ResponseEntity<?> handler(NoResourceFoundException ex, HttpServletRequest request){
        return ResponseEntity.badRequest().body(ErrorMessage.builder()
                .timestamp(new Date())
                .code(HttpStatus.NOT_FOUND)
                .status(HttpServletResponse.SC_BAD_REQUEST)
                .url(request.getRequestURL().toString())
                .errors(ex.getMessage()).build());
    }


}
