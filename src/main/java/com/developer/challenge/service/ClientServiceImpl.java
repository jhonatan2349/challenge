package com.developer.challenge.service;

import com.developer.challenge.domain.dto.ClientDTO;
import com.developer.challenge.domain.entity.Client;
import com.developer.challenge.domain.repository.ClientRepository;
import com.developer.challenge.exception.CustomException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Page<ClientDTO> getAllClients(Pageable pageable) {
        Page<Client> clients = clientRepository.findAll(pageable);
        return clients.map(ClientDTO::of);
    }

    @Override
    public Optional<ClientDTO> getClientById(Long id) {
        return clientRepository.findById(id).map(ClientDTO::of);
    }

    @Override
    public ClientDTO saveClient(ClientDTO dto) {
        Client entity = new Client();
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        return ClientDTO.of(clientRepository.save(entity));
    }

    @Override
    public ClientDTO updateClient(ClientDTO dto, Long id) {
        Optional<Client> optionalClient = clientRepository.findById(id);
        if (optionalClient.isPresent()) {
            Client client = optionalClient.get();
            client.setName(dto.getName());
            client.setEmail(dto.getEmail());
            return ClientDTO.of(clientRepository.save(client));
        } else {
            throw new CustomException("El cliente con el ID proporcionado no existe");
        }
    }

    @Override
    public void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }
}
