package com.developer.challenge.service;

import com.developer.challenge.domain.dto.AddressDTO;
import com.developer.challenge.domain.entity.Address;
import com.developer.challenge.domain.repository.AddressRepository;
import com.developer.challenge.exception.CustomException;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Page<AddressDTO> getAllAddresses(Pageable pageable) {
        Page<Address> addressPage = addressRepository.findAll(pageable);
        List<AddressDTO> addressDTOs = addressPage
                .stream()
                .map(AddressDTO::of)
                .collect(Collectors.toList());
        return new PageImpl<>(addressDTOs, pageable, addressPage.getTotalElements());
    }

    @Override
    public Optional<AddressDTO> getAddressById(Long id) {
        return addressRepository.findById(id).map(AddressDTO::of);
    }

    @Override
    public AddressDTO updateAddress(AddressDTO dto, Long id) {
        Optional<Address> optionalAddress = addressRepository.findById(id);
        if (optionalAddress.isPresent()) {
            Address existingAddress = optionalAddress.get();
            BeanUtils.copyProperties(dto, existingAddress, "id", "client");

            existingAddress.setClient(optionalAddress.get().getClient());

            Address updatedAddress = addressRepository.save(existingAddress);
            return AddressDTO.of(updatedAddress);
        } else {
            throw new CustomException("El id de esta dirección no existe");
        }
    }

    @Override
    public AddressDTO saveAddress(AddressDTO dto) {
        Address entity = dto.toEntity();
        return AddressDTO.of(addressRepository.save(entity));
    }

    @Override
    public void deleteAddress(Long id) {
        if (!addressRepository.existsById(id)) {
            throw new CustomException("El id de esta dirección no existe");
        }
        addressRepository.deleteById(id);
    }
}
