package com.developer.challenge.service;

import com.developer.challenge.domain.dto.ClientDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ClientService {
    Page<ClientDTO> getAllClients(Pageable pageable);
    Optional<ClientDTO> getClientById(Long id);
    ClientDTO saveClient(ClientDTO dto);
    ClientDTO updateClient(ClientDTO dto, Long id);
    void deleteClient(Long id);
}
