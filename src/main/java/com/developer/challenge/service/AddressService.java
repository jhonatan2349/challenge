package com.developer.challenge.service;

import com.developer.challenge.domain.dto.AddressDTO;
import com.developer.challenge.domain.entity.Address;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AddressService {
    Page<AddressDTO> getAllAddresses(Pageable pageable);
    Optional<AddressDTO> getAddressById(Long id);
    AddressDTO saveAddress(AddressDTO dto);
    AddressDTO updateAddress(AddressDTO dto, Long id);
    void deleteAddress(Long id);
}
