package com.developer.challenge.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "Client Api",
                version = "1.0.0",
                description = "This is a service for management clients"
        )
)
public class OpenApiConfig {
}
