package com.developer.challenge.resource;

import com.developer.challenge.domain.dto.AddressDTO;
import com.developer.challenge.service.AddressService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@Tag(name = "Address Resource")
@RestController
@RequestMapping("/api/addresses")
public class AddressResource {

    private final AddressService addressService;

    public AddressResource(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    public Page<AddressDTO> getAllAddresses(Pageable pageable) {
        return addressService.getAllAddresses(pageable);
    }

    @Operation(summary = "Get an address by ID.")
    @GetMapping("/{id}")
    public ResponseEntity<AddressDTO> getAddressById(@PathVariable Long id) {
        log.info("GET: address ID: {}", id);
        Optional<AddressDTO> address = addressService.getAddressById(id);
        return address.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<AddressDTO> createAddress(@RequestBody AddressDTO dto) {
        AddressDTO createdAddress = addressService.saveAddress(dto);
        return ResponseEntity.ok(createdAddress);
    }

    @Operation(summary = "Update an address by ID.")
    @PutMapping("/{id}")
    public ResponseEntity<AddressDTO> updateAddress(@PathVariable Long id, @RequestBody AddressDTO addressDetails) {
        log.info("PUT: update address ID: {}", id);
        AddressDTO updatedAddress = addressService.updateAddress(addressDetails, id);
        return ResponseEntity.ok(updatedAddress);
    }

    @Operation(summary = "Delete an address by ID.")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAddress(@PathVariable Long id) {
        log.info("DELETE: delete address ID: {}", id);
        if (addressService.getAddressById(id).isPresent()) {
            addressService.deleteAddress(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
