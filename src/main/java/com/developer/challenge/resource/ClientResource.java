package com.developer.challenge.resource;

import com.developer.challenge.domain.dto.ClientDTO;
import com.developer.challenge.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@Tag(name = "Client Resource")
@RestController
@RequestMapping("/api/clients")
public class ClientResource {

    private final ClientService clientService;

    public ClientResource(ClientService clientService) {
        this.clientService = clientService;
    }

    @Operation(summary = "Get all clients with pagination.")
    @GetMapping
    public Page<ClientDTO> getAllClients(Pageable pageable) {
        log.info("GET: Get all clients");
        return clientService.getAllClients(pageable);
    }

    @Operation(summary = "Get a client by ID.")
    @GetMapping("/{id}")
    public ResponseEntity<ClientDTO> getClientById(@PathVariable Long id) {
        log.info("GET: Get client by ID: {}", id);
        Optional<ClientDTO> client = clientService.getClientById(id);
        return client.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(summary = "Create a new client.")
    @PostMapping
    public ResponseEntity<ClientDTO> createClient(@RequestBody ClientDTO clientDTO) {
        log.info("POST: Create client with name: {}", clientDTO.getName());
        ClientDTO createdClient = clientService.saveClient(clientDTO);
        return ResponseEntity.ok(createdClient);
    }

    @Operation(summary = "Update a client by ID.")
    @PutMapping("/{id}")
    public ResponseEntity<ClientDTO> updateClient(@PathVariable Long id, @RequestBody ClientDTO clientDTO) {
        log.info("PUT: Update client with ID: {}", id);
        ClientDTO updatedClient = clientService.updateClient(clientDTO, id);
        return ResponseEntity.ok(updatedClient);
    }

    @Operation(summary = "Delete a client by ID.")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable Long id) {
        log.info("DELETE: Delete client with ID: {}", id);
        if (clientService.getClientById(id).isPresent()) {
            clientService.deleteClient(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
