package com.developer.challenge.domain.repository;

import com.developer.challenge.domain.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
