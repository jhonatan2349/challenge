package com.developer.challenge.domain.repository;

import com.developer.challenge.domain.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
