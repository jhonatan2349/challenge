package com.developer.challenge.domain.dto;

import com.developer.challenge.domain.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO implements Serializable {

    private Long id;
    private String street;
    private String city;
    private String state;
    private String zipCode;
    private ClientDTO clientDTO;

    public static AddressDTO of(Address address) {
        return AddressDTO.builder()
                .id(address.getId())
                .street(address.getStreet())
                .city(address.getCity())
                .state(address.getState())
                .zipCode(address.getZipCode())
                .clientDTO(ClientDTO.of(address.getClient()))
                .build();
    }

    public Address toEntity() {
        return Address.builder()
                .id(id)
                .street(street)
                .city(city)
                .state(state)
                .zipCode(zipCode)
                .client(clientDTO.toEntity())
                .build();
    }
}
