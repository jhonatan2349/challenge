package com.developer.challenge.domain.dto;

import com.developer.challenge.domain.entity.Client;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO implements Serializable {

    private Long id;
    private String name;
    private String email;
    private List<AddressDTO> addressesDto;

    public static ClientDTO of(Client client) {
        return ClientDTO.builder()
                .id(client.getId())
                .name(client.getName())
                .email(client.getEmail())
                .addressesDto(client.getAddresses() != null ?
                        client.getAddresses().stream().map(AddressDTO::of).collect(Collectors.toList()) : null)
                .build();
    }

    public Client toEntity() {
        return Client.builder()
                .id(id)
                .name(name)
                .email(email)
                .addresses(addressesDto != null ?
                        addressesDto.stream().map(AddressDTO::toEntity).collect(Collectors.toList()) : null)
                .build();
    }
}
