package com.developer.challenge;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Optional;

import com.developer.challenge.domain.dto.ClientDTO;
import com.developer.challenge.domain.entity.Address;
import com.developer.challenge.domain.entity.Client;
import com.developer.challenge.domain.repository.ClientRepository;
import com.developer.challenge.service.ClientServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientServiceImpl clientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetClientById() {
        Client client = new Client(1L, "John Doe", "john.doe@example.com", getListAddress());
        when(clientRepository.findById(1L)).thenReturn(Optional.of(client));

        Optional<ClientDTO> foundClient = clientService.getClientById(1L);

        assertTrue(foundClient.isPresent());
        assertEquals("John Doe", foundClient.get().getName());
        verify(clientRepository, times(1)).findById(1L);
    }

    @Test
    void testDeleteClient() {
        clientService.deleteClient(1L);
        verify(clientRepository, times(1)).deleteById(1L);
    }

    private List<Address> getListAddress(){
        return List.of(Address.builder()
                .id(1L)
                .client(Client
                        .builder()
                                .id(1L)
                        .addresses(List.of())
                                .build())
                .build());
    }
}
