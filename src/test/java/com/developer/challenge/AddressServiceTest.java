package com.developer.challenge;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import java.util.List;
import java.util.Optional;

import com.developer.challenge.domain.dto.AddressDTO;
import com.developer.challenge.domain.entity.Address;
import com.developer.challenge.domain.entity.Client;
import com.developer.challenge.domain.repository.AddressRepository;
import com.developer.challenge.service.AddressServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class AddressServiceTest {

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private AddressServiceImpl addressService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllAddresses() {

        Address address1 = new Address(1L, "123 Main St", "Springfield", "IL", "62701", getClieng());
        Address address2 = new Address(2L, "456 Elm St", "Springfield", "IL", "62701", getClieng());

        List<Address> addressList = Arrays.asList(address1, address2);
        Page<Address> page = new PageImpl<>(addressList);
        when(addressRepository.findAll(Pageable.ofSize(2))).thenReturn(page);

        Page<AddressDTO> addresses = addressService.getAllAddresses(Pageable.ofSize(2));

        assertEquals(2, addresses.getTotalElements());
        verify(addressRepository, times(1)).findAll(Pageable.ofSize(2));
    }

    @Test
    void testGetAddressById() {
        Address address = new Address(1L, "123 Main St", "Springfield", "IL", "62701", getClieng());
        when(addressRepository.findById(1L)).thenReturn(Optional.of(address));

        Optional<AddressDTO> foundAddress = addressService.getAddressById(1L);

        assertTrue(foundAddress.isPresent());
        assertEquals("123 Main St", foundAddress.get().getStreet());
        verify(addressRepository, times(1)).findById(1L);
    }

    @Test
    void testDeleteAddress() {
        addressService.deleteAddress(1L);
        verify(addressRepository, times(1)).deleteById(1L);
    }

    private Client getClieng(){
        return Client.builder()
                .name("jonathna")
                .id(1L)
                .addresses(List.of())
                .build();
    }
}
